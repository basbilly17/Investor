# Generated by Django 2.2.5 on 2019-09-15 12:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account_user', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='user_type',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Admin'), (2, 'User')], default=2, null=True),
        ),
    ]
