from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.
class Account(AbstractUser):
    USER_TYPE_CHOICES = (
        (1, 'Admin'),
        (2, 'User')
    )
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE_CHOICES, null=True, default=2)
