from django.urls import path, include
from rest_framework import routers
from .views_log_in import LogInView, LogOutView

router = routers.DefaultRouter()
router.register('login', LogInView)
router.register('logout', LogOutView)

app_name = 'account'
urlpatterns = [
    path('', include(router.urls)),
]
