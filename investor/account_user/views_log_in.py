import re

from django.contrib.auth import login, authenticate
from django.contrib.auth import logout
from rest_framework import mixins
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .models import Account
from .serializers_log_in import LogInSerializer


class LogInView(mixins.CreateModelMixin, GenericViewSet):
    queryset = Account.objects.none()
    serializer_class = LogInSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        username = request.data['user_name']
        password = request.data['password']
        regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$'
        if re.search(regex, username):
            account = Account.objects.filter(email=username).first()
            if account:
                username = account.username
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return Response(data={'massage': 'log in complete'}, status=status.HTTP_200_OK)
        else:
            return Response(data={'massage': 'wrong username or password'}, status=status.HTTP_400_BAD_REQUEST)


class LogOutView(mixins.ListModelMixin, GenericViewSet):
    queryset = Account.objects.none()

    def list(self, request, *args, **kwargs):
        logout(request)
        return Response(data={'massage': 'log out complete'}, status=status.HTTP_200_OK)
