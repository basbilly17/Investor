from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

urlpatterns_api_user = [
    path('api/account/', include('account_user.urls')),
]

urlpatterns = [
    path('api/', get_swagger_view(title='API Docs.', patterns=urlpatterns_api_user)),
    path('auth/', include('allauth.urls')),
    path('admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('accounts/', include('django.contrib.auth.urls'))

    # http://localhost:8000/auth/facebook/login/?process=login
    # http://localhost:8000/auth/google/login/
]

urlpatterns += urlpatterns_api_user
